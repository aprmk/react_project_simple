import React, {useEffect, useState} from 'react';
import {styles} from "./FormStyles";

const Form = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const [emailDirty, setEmailDirty] = useState(false)
    const [passwordDirty, setPasswordDirty] = useState(false)

    const [emailError, setEmailError] = useState('NOT EMPTY')
    const [passwordError, setPasswordError] = useState('NOT EMPTY')

    const [formValid, setFormValid] = useState(false)

    useEffect(() => {
        if (emailError || passwordError) {
            setFormValid(false)
        } else {
            setFormValid(true)
        }
    }, [emailError, passwordError])

    const user = {
        login: process.env.REACT_APP_EMAIL,
        password: process.env.REACT_APP_PASSWORD
    }

    const userCheck =  (email, password) => {
        if ((email === user.login) && (password === user.password)) {
            localStorage.setItem(email, password);
            window.location.href = "/profile"
        }else{
            if ((email !== user.login) || (password !== user.login)) {
                localStorage.clear()
                setEmailError('Wrong email')
                setPasswordError('Wrong password')
            }
            console.log("not user")
        }
    }

    const emailHandler = (e) => {
        setEmail(e.target.value)
        const regular = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (!regular.test(String(e.target.value).toLowerCase())) {
            setEmailError('Email must have @')
        } else {
            setEmailError("")
        }
    }


    const passwordHandler = (e) => {
        setPassword(e.target.value)
        if (e.target.value.length < 3 || e.target.length > 20)
        {
            setPasswordError('Password longer than 3 and shorter than 20')
            if (!e.target.value) {
                setPasswordError('Password cannot be empty')
            }
            } else {
                setPasswordError('')
            }
    }

    const blurHandler = (e) => {
        switch (e.target.name) {
            case 'email':
                setEmailDirty(true)
                break
            case 'password':
                setPasswordDirty(true)
                break
        }
    }

    return (
        <div className='container_form_large'>
            <form>
                <div className='container_form_little'>
                    <h1>REGISTRATION</h1>
                    <div className='child'>
                        {(emailDirty && emailError) && <div style={{color: 'red', fontSize: 13}}>{emailError}</div>}
                        <input style={styles.input} onChange={e => emailHandler(e)} value={email}
                               onBlur={e => blurHandler(e)} name='email'
                               type='text' size='40'
                               placeholder='Enter your email...'/>
                    </div>
                    <div className='child'>
                        {(passwordDirty && passwordDirty) &&
                        <div style={{color: 'red', fontSize: 13}}>{passwordError}</div>}
                        <input style={styles.input} onChange={e => passwordHandler(e)} value={password}
                               onBlur={e => blurHandler(e)} name='password'
                               type='text' size='40'
                               placeholder='Enter your password...'/>
                    </div>
                    <button onClick={e => userCheck(email, password) }  className='child' disabled={!formValid} type='button'>Registration</button>
                </div>
            </form>
        </div>

    );
};
export default Form