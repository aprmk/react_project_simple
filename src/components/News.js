import React from 'react';
import firstPhoto from "./img/img_1.jpg";
import secondPhoto from "./img/img_2.jpg";
import thirdPhoto from "./img/img_3.jpg";
import fourthPhoto from "./img/img_4.jpg";
import fifthPhoto from "./img/img_5.jpg";
import sixthPhoto from "./img/img_6.jpg";
import NewsList from "./NewsList";

const News = () => {

    const news_list = [
        { id:1, pulicatedDate: Date, author:"Vladimir Nabokov", header:"Why the coronavirus numbers are likely", imagePath: firstPhoto,
            all_content:"In fact, we're looking at some of the worst coronavirus numbers in a long time," +
                " and, unlike earlier this year, it's not clear at all that there is the public will " +
                "to do what's necessary to slow down the rate of infection.\n" +
                "A glance at the numbers tells the story. Right now, the virus is raging in" +
                " pretty much every state. As of this writing, a CNN analysis of Johns" +
                " Hopkins University data indicates that the number of coronavirus cases " +
                "is up in every state compared to last week, except for Georgia. " +
                "A New York Times examination of the data shows that in over 90% " +
                "of the states, there was a daily average of at least 15 new cases " +
                "per 100,000 people over the past week."
        },
        { id:2, pulicatedDate: Date, author:"Barack Obama", header:"States enact more rules now", imagePath: secondPhoto, content:"contentNews2",
            all_content:"If everyone took precautions and stopped assuming their friends aren't infected, the results could be more effective than a lockdown, Los Angeles Mayor Eric Garcetti said.\n" +
                "These charts show how serious this fall&#39;s Covid-19 surge is\n" +
                "These charts show how serious this fall's Covid-19 surge is\n" +
                "\"The good thing about where we are now is we're smarter than we were in March. " +
                "We understand that this blanket kind of lockdown, which did the trick then, may not be the best way now,\"" +
                " he said.\n" +
                "\"It's not about whether a store is open or not. It's about your and my behavior." +
                " It's about whether we think, 'Oh, I know that person, so I'm familiar with them. " +
                "I can hang out with them ... Those things are what's causing the spread.\"\n" +
                "So if people don't like shutdowns, the solutions are simple: \"Cancel those vacation " +
                "plans right now. Do not sneak in other households for Thanksgiving,\" Garcetti said.\n" +
                "\"To me, the mantra is two things: Don't share your air, and don't do stupid things.\"",
             },
        { id:3, pulicatedDate: Date, author:"Joseph Biden", header:"Coronavirus: What's happening in Canada", imagePath: thirdPhoto, content:"contentNews3",
            all_content: "Canada's chief public health officer is cautioning Canadians against " +
                "\"letting our guard down\" in the fight against COVID-19 as holiday season approaches and colder weather " +
                "drives people indoors.\n" +
                "\n" +
                "Dr. Theresa Tam said in a statement on Sunday that hot spots across the country are showing " +
                "that social gatherings and crowded places where social distancing is made difficult \"can " +
                "amplify spread of the virus,\" and urged Canadians follow public health guidelines as they" +
                " hunker down for winter and celebrate the holidays.\n" +
                "\n" +
                "\"In these more relaxed settings, such as family and holiday celebrations and recreational " +
                "activities, letting our guard down and not consistently maintaining public health " +
                "practices can lead to many exposures and infections,\" Tam said.\n" +
                "\n" +
                "Tam also suggested Canadians wear masks indoors when gathering with people " +
                "from outside their immediate bubble, consider socializing virtually and \"bundle up to" +
                " keep more activities outdoors as the weather cools.\"" },
        { id:4, pulicatedDate: Date, author:"Ron Paul", header:"As Trump ignores deepening coronavirus", imagePath: fourthPhoto, content:"contentNews5",
            all_content: "(CNN)Joe Biden has spent these early days as President-elect pleading with Americans to pay attention to the relentless surge of Covid-19" +
                " -- with deaths averaging more than 1,000 a day in the past week -- as President Donald Trump continues to ignore the deepening crisis and touts the promises of yet-to-be-approved vaccines as his panacea.\n" +
                "\n" +
                "The continuing power struggle between two men with diametrically different philosophies on how the US should handle the virus has left the nation rudderless at this critical moment -- forced by Trump into a governing crisis as he refuses to let the transition to the Biden presidency proceed and pass on knowledge that could be critical to slowing the spread of the virus next year.\n" +
                "This past week, some Republicans in Congress finally seemed to take note of how the President's blockade was threatening national security -- as Republican Sens. James Lankford of Oklahoma, Chuck Grassley of Iowa and Senate Majority Whip John Thune of South Dakota, among others, stepped up to say that the President-elect should begin receiving intelligence briefings as is customary during the transition of power." },
        { id:5, pulicatedDate: Date, author:"John Bush", header:"Coronavirus cases surge in Indiana", imagePath: fifthPhoto, content:"contentNews5",
            all_content: "New coronavirus cases leaped in Indiana in the week ending Saturday, rising 48.6% as 39,165 cases were reported. The previous week had 26,364 new cases.\n" +
                "\n" +
                "Indiana ranked No. 12 among the states where coronavirus was spreading the fastest, a USA TODAY Network analysis of Johns Hopkins University data shows. In the latest week the United States added 1,017,810 reported cases of coronavirus, an increase of 33.3% from the week before. Across the country, 48 states had more cases in the latest week than they did in the week before.\n" +
                "\n" +
                "Within Indiana, the worst weekly outbreaks on a per-person basis were in Elkhart, Marshall and Randolph counties. Adding the most new cases overall were Marion County, with 4,632 cases; Lake County," +
                " with 3,645 cases; and Allen County, with 2,672. Weekly case counts rose in 89 counties from the previous week. The worst increases from the prior week's pace were in Marion, Allen and Lake counties." },
        { id:6, pulicatedDate: Date, author:"Mitt Romney", header:"Despite million coronavirus cases", imagePath: sixthPhoto, content:"contentNews6",
            all_content: "HOUSTON — The map of Texas lays bare the unrelenting march of the pandemic.\n" +
                "\n" +
                "A new coronavirus surge is driven by outbreaks in cities, towns and wind-swept outposts, stretching from Dallas to west Texas. Health officials along the border in El Paso extended a stay-home order, more than doubled their mobile morgues to 10 and expanded a temporary hospital at the convention center. Fresh outbreaks were piling up in Amarillo and Lubbock, too.\n" +
                "\n" +
                "Big cities were also hit hard, but because the infections were spread across larger populations — the Houston metro area alone is home to about 7 million people — they didn’t trigger lockdowns or public outcry. By the end of a disquieting and startling week one thing was clear: Texas, like much of the nation, is overwhelmed by a virus that is more stubborn than the will of many quarantine-fatigued Americans who want to wish it away." },
    ]

    return (
        <div>
            <NewsList news={news_list}/>
        </div>
    );
};


export default News;