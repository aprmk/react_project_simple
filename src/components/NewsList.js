import React from 'react'
import '../App.css'


const NewsList = ({news, header}) => {

    return (

<div>
    <div className='news_header'>{header}</div>
    <div className= 'news_content'>
    {news.map(new_s =>
        <div className='news_block'>
               <img className="news_image" src={new_s.imagePath}/>
                <p className="news_header_one">{new_s.header}</p>
                <p className="news_all_content">{new_s.all_content.substr(0,100)}...</p>
                <p>{new_s.pulicatedDate}</p>
        </div>

    )}
    </div>
</div>
    )};

export default NewsList;

