import React from 'react';
import NewsList from "./NewsList";

import firstPhoto from './img/img_1.jpg';
import secondPhoto from './img/img_2.jpg';
import thirdPhoto from './img/img_3.jpg';


const Main = () => {

    const news = [

        { id:1, pulicatedDate: Date, author:"Vladimir Nabokov", header:"Why the coronavirus numbers are likely", imagePath: firstPhoto, content: "contentNews1" ,
            all_content:"In fact, we're looking at some of the worst coronavirus numbers in a long time," +
                " and, unlike earlier this year, it's not clear at all that there is the public will " +
                "to do what's necessary to slow down the rate of infection.\n" +
                "A glance at the numbers tells the story. Right now, the virus is raging in" +
                " pretty much every state. As of this writing, a CNN analysis of Johns" +
                " Hopkins University data indicates that the number of coronavirus cases " +
                "is up in every state compared to last week, except for Georgia. " +
                "A New York Times examination of the data shows that in over 90% " +
                "of the states, there was a daily average of at least 15 new cases " +
                "per 100,000 people over the past week.",
        },
        { id:2, pulicatedDate: Date, author:"Barack Obama", header:"States enact more rules now", imagePath: secondPhoto, content:"contentNews2",
            all_content:"If everyone took precautions and stopped assuming their friends aren't infected, the results could be more effective than a lockdown, Los Angeles Mayor Eric Garcetti said.\n" +
                "These charts show how serious this fall&#39;s Covid-19 surge is\n" +
                "These charts show how serious this fall's Covid-19 surge is\n" +
                "\"The good thing about where we are now is we're smarter than we were in March. " +
                "We understand that this blanket kind of lockdown, which did the trick then, may not be the best way now,\"" +
                " he said.\n" +
                "\"It's not about whether a store is open or not. It's about your and my behavior." +
                " It's about whether we think, 'Oh, I know that person, so I'm familiar with them. " +
                "I can hang out with them ... Those things are what's causing the spread.\"\n" +
                "So if people don't like shutdowns, the solutions are simple: \"Cancel those vacation " +
                "plans right now. Do not sneak in other households for Thanksgiving,\" Garcetti said.\n" +
                "\"To me, the mantra is two things: Don't share your air, and don't do stupid things.\"",
            },
        { id:3, pulicatedDate: Date, author:"Joseph Biden", header:"Coronavirus: What's happening in Canada", imagePath: thirdPhoto, content:"contentNews3",
            all_content: "Canada's chief public health officer is cautioning Canadians against " +
                "\"letting our guard down\" in the fight against COVID-19 as holiday season approaches and colder weather " +
                "drives people indoors.\n" +
                "\n" +
                "Dr. Theresa Tam said in a statement on Sunday that hot spots across the country are showing " +
                "that social gatherings and crowded places where social distancing is made difficult \"can " +
                "amplify spread of the virus,\" and urged Canadians follow public health guidelines as they" +
                " hunker down for winter and celebrate the holidays.\n" +
                "\n" +
                "\"In these more relaxed settings, such as family and holiday celebrations and recreational " +
                "activities, letting our guard down and not consistently maintaining public health " +
                "practices can lead to many exposures and infections,\" Tam said.\n" +
                "\n" +
                "Tam also suggested Canadians wear masks indoors when gathering with people " +
                "from outside their immediate bubble, consider socializing virtually and \"bundle up to" +
                " keep more activities outdoors as the weather cools.\""
        },
    ]
    return (
            <div>
                <NewsList header={"News"} news={news}/>
            </div>

    );
};

export default Main


